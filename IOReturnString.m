
#import <IOKit/IOReturn.h>
#import "IOReturnString.h"


const char * ioReturnString(uint32_t code) {
	const char * result;
	
	switch (code) {
		case kIOReturnSuccess:
			result = "OK";
			break;
		case kIOReturnError:
			result = "general error";
			break;
		case kIOReturnNoMemory:
			result = "can't allocate memory";
			break;
		case kIOReturnNoResources:
			result = "resource shortage";
			break;
		case kIOReturnIPCError:
			result = "error during IPC";
			break;
		case kIOReturnNoDevice:
			result = "no such device";
			break;
		case kIOReturnNotPrivileged:
			result = "privilege violation";
			break;
		case kIOReturnBadArgument:
			result = "invalid argument";
			break;
		case kIOReturnLockedRead:
			result = "device read locked";
			break;
		case kIOReturnLockedWrite:
			result = "device write locked";
			break; 
		case kIOReturnExclusiveAccess:
			result = "exclusive access and device already open";
			break; 
		case kIOReturnBadMessageID:
			result = "sent/received messages had different msg_id";
			break;
		case kIOReturnUnsupported:
			result = "unsupported function";
			break;
		case kIOReturnVMError:
			result = "misc. VM failure";
			break;
		case kIOReturnInternalError:
			result = "internal error";
			break;
		case kIOReturnIOError:
			result = "General I/O error";
			break;
		case kIOReturnCannotLock:
			result = "can't acquire lock";
			break;
		case kIOReturnNotOpen:
			result = "device not open";
			break;
		case kIOReturnNotReadable:
			result = "read not supported";
			break; 
		case kIOReturnNotWritable:
			result = "write not supported";
			break; 
		case kIOReturnNotAligned:
			result = "alignment error";
			break;
		case kIOReturnBadMedia:
			result = "Media Error";
			break;
		case kIOReturnStillOpen:
			result = "device(s) still open";
			break;
		case kIOReturnRLDError:
			result = "rld failure";
			break;
		case kIOReturnDMAError:
			result = "DMA failure";
			break;
		case kIOReturnBusy:
			result = "Device Busy";
			break;
		case kIOReturnTimeout:
			result = "I/O Timeout";
			break;
		case kIOReturnOffline:
			result = "device offline";
			break;
		case kIOReturnNotReady:
			result = "not ready";
			break;
		case kIOReturnNotAttached:
			result = "device not attached";
			break;
		case kIOReturnNoChannels:
			result = "no DMA channels left";
			break;
		case kIOReturnNoSpace:
			result = "no space for data";
			break;
		case kIOReturnPortExists:
			result = "port already exists";
			break;
		case kIOReturnCannotWire:
			result = "can't wire down physical memory";
			break;
		case kIOReturnNoInterrupt:
			result = "no interrupt attached";
			break;
		case kIOReturnNoFrames:
			result = "no DMA frames enqueued";
			break;
		case kIOReturnMessageTooLarge:
			result = "oversized msg received on interrupt port";
			break;
		case kIOReturnNotPermitted:
			result = "not permitted";
			break;
		case kIOReturnNoPower:
			result = "no power to device";
			break;
		case kIOReturnNoMedia:
			result = "media not present";
			break;
		case kIOReturnUnformattedMedia:
			result = "media not formatted";
			break;
		case kIOReturnUnsupportedMode:
			result = "no such mode";
			break;
		case kIOReturnUnderrun:
			result = "data underrun";
			break;
		case kIOReturnOverrun:
			result = "data overrun";
			break;
		case kIOReturnDeviceError:
			result = "the device is not working properly!";
			break;
		case kIOReturnNoCompletion:
			result = "a completion routine is required";
			break;
		case kIOReturnAborted:
			result = "operation aborted";
			break;
		case kIOReturnNoBandwidth:
			result = "bus bandwidth would be exceeded";
			break;
		case kIOReturnNotResponding:
			result = "device not responding";
			break;
		case kIOReturnIsoTooOld:
			result = "isochronous I/O request for distant past!";
			break;
		case kIOReturnIsoTooNew:
			result = "isochronous I/O request for distant future";
			break;
		case kIOReturnNotFound:
			result = "data was not found";
			break;
		case kIOReturnInvalid:
			result = "should never be seen";
			break;
		default:
			result = "unrecognized code";
	}
	
	return result;
}