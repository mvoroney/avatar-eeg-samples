
#include <stdio.h>
#import <IOBluetooth/objc/IOBluetoothRFCOMMChannel.h>

#import <string.h>
#import <stdlib.h>
#import <stdint.h>

#import "EEGRFCommChannelDelegate.h"

// raw structures
struct ERP_timing_struct {
    uint32_t SOC;
    uint32_t counter;
    uint32_t frame_count;
    uint32_t reserved[3];
};

struct ERP_channel {
    uint8_t data[3];
};

struct ERP_channel_set {
    struct ERP_channel channels[8];
};

struct ERP_frame_header {
    uint8_t sync;
    uint8_t version;
    uint16_t framesize;
    uint8_t frametype;
    uint32_t framecount;
    uint8_t channels;
    uint16_t samples; 
};


@implementation EEGRFCommChannelDelegate

-(id) init {
    self = [super init];
    
    if (nil != self) {
        remainingSamples = 0;
    }
    
    return self;
}

- (void)rfcommChannelData:(IOBluetoothRFCOMMChannel*)rfcommChannel data:(void *)dataPointer length:(size_t)dataLength {
    printf("%d: channel data recieved.\n", __LINE__);
    
    struct ERP_frame_header * header;
    // data for all channels.
    struct ERP_channel * channelData = dataPointer;
    struct ERP_timing_struct * timing;
    
    // Note: possible data loss due to differing alignments of dataPointer and channelData.
    while (remainingSamples > 0 && (void*)channelData < dataPointer + dataLength) {
        /// \todo what order should we output the bytes in?
        printf("|0x%x %x %x|", channelData->data[0], channelData->data[1], channelData->data[2]);
        remainingSamples--;
        channelData++;
    }
    
    //dataPointer = (void*)channelData;
    if ((void*)channelData < dataPointer + dataLength) {
        header = (struct ERP_frame_header*)channelData;
        
        // find sync byte
        if (0xAA != header->sync) printf("syncing: ");
        while (0xAA != header->sync && (void*)header < dataPointer + dataLength) {
            header = (struct ERP_frame_header*)(((uint8_t*)header) + 1);
            putchar('.');
        }
        
        
        channelData = (struct ERP_channel*)(((uint8_t*)header) + sizeof(struct ERP_frame_header));
        
        if ((void*)header < dataPointer + dataLength) {
            assert(0xAA == header->sync && "expected sync byte not found.");
            remainingSamples = header->samples;
    
            // let's hope we never get less than a full header worth of data.
            printf("header: {sync: 0x%x version: 0x%x framesize: %d frametype: %d framecount: %u channels: %d samples: %d}\n", 
                   header->sync, header->version, header->framesize, header->frametype, header->framecount, header->channels, header->samples);
            
            // check to see if this dataframe includes timing info.
            if (0xF0000000 & header->framecount) {
                timing = (struct ERP_timing_struct*)(((uint8_t*)header) + sizeof(struct ERP_frame_header));
                channelData = (struct ERP_channel*)(((uint8_t*)timing) + sizeof(struct ERP_timing_struct));
                
                // let's hope we never get less than a full header worth of data.
                printf("timing info: {SOC: %u counter: %u frame count: %u}\n", 
                       timing->SOC, timing->counter, timing->frame_count);
            }
        }
        
        // Note: possible data loss due to differing alignments of dataPointer and channelData.
        while (remainingSamples > 0 && (void*)channelData < dataPointer + dataLength) {
            /// \todo what order should we output the bytes in?
            printf("|0x%x %x %x|", channelData->data[0], channelData->data[1], channelData->data[2]);
            remainingSamples--;
            channelData++;
        }
    }
    
}

- (void)rfcommChannelOpenComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel status:(IOReturn)error {
    assert(kIOReturnSuccess == error);
    printf("%d: opened channel successfully.\n", __LINE__);
}

- (void)rfcommChannelClosed:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    printf("%d: closed channel successfully.\n", __LINE__);
    CFRunLoopStop(CFRunLoopGetCurrent());
}

- (void)rfcommChannelControlSignalsChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    printf("%d: control signal changed.\n", __LINE__);
}

- (void)rfcommChannelFlowControlChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    printf("%d: flow control changed.\n", __LINE__);
}


- (void)rfcommChannelQueueSpaceAvailable:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    printf("%d: queue space available.\n", __LINE__);
}

@end
