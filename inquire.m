#include <stdio.h>

#import <Foundation/NSObject.h>
#import <Foundation/Foundation.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSEnumerator.h>
#import <IOkit/IOReturn.h>

#import <IOBluetooth/objc/IOBluetoothDevice.h>
#import <IOBluetooth/objc/IOBluetoothDeviceInquiry.h>
#import <IOBluetooth/objc/IOBluetoothSDPServiceRecord.h>

@interface Discoverer : NSObject
    {}

    - (void)deviceInquiryComplete:(IOBluetoothDeviceInquiry *)sender error:(IOReturn)error aborted:(BOOL)aborted;
    - (void)deviceInquiryDeviceFound:(IOBluetoothDeviceInquiry *)sender device:(IOBluetoothDevice *)device;
@end

@implementation Discoverer

    - (void)deviceInquiryComplete:(IOBluetoothDeviceInquiry *)sender error:(IOReturn)error aborted:(BOOL)aborted {
        CFRunLoopStop(CFRunLoopGetCurrent());
    }

    - (void)deviceInquiryDeviceFound:(IOBluetoothDeviceInquiry *)sender device:(IOBluetoothDevice *)device {
        printf("%d: discovered %s name=%s, majorService=%x, majorClass=%x, minorClass=%x\n", 
               __LINE__, 
               [[device getAddressString] UTF8String], 
               [[device name] UTF8String],
               [device getServiceClassMajor],
               [device getDeviceClassMajor],
               [device getDeviceClassMinor]
        );
    }

@end

@interface SDPQuery : NSObject
- (void)sdpQueryComplete:(IOBluetoothDevice*) dev status: (IOReturn) stat;

@end

@implementation SDPQuery

- (void)sdpQueryComplete:(IOBluetoothDevice*) dev status: (IOReturn) stat {
    assert(kIOReturnSuccess == stat);
    printf("%d: sdp query completed successfully.\n", __LINE__);
    CFRunLoopStop(CFRunLoopGetCurrent());
}

@end

int main (int argc, const char * argv[]) {
    
    // works with xcode 3
    NSAutoreleasePool * pool = [NSAutoreleasePool new];
    
    // needs xcode 4
    //@autoreleasepool {
        
    IOReturn result;
    Discoverer *dis = [[Discoverer new] autorelease];
    NSMutableArray *devs;
    NSEnumerator *denum;
    int i;
    IOBluetoothDevice *device;
    IOBluetoothDeviceInquiry *diref = [[IOBluetoothDeviceInquiry new] autorelease];
    
    [diref setDelegate:dis];
    [diref setInquiryLength:10];
    /*
    [diref setSearchCriteria:kBluetoothServiceClassMajorAny 
            majorDeviceClass:kBluetoothDeviceClassMajorUnclassified 
            minorDeviceClass:kBluetoothDeviceClassMinorComputerUnclassified];
     */

    result = [diref start];
    assert(kIOReturnSuccess == result);
    printf("%d: start succeeded.\n", __LINE__);

    // start device search loop.
    CFRunLoopRun();
        
    result = [diref stop];
    assert(kIOReturnSuccess == result);
    printf("%d: stop succeeded.\n", __LINE__);
    
    // make a mutable array from the results so it can be manipulated.
    devs = [[NSMutableArray arrayWithArray: [diref foundDevices]] autorelease];
    
    // open conneciton
    // NB: [devs makeObjectsPerformSelector: @selector(openConnection)]; also works.
    for (i = 0; i < [devs count]; i++) {
        device = [devs objectAtIndex: i];
        result = [device openConnection];
        
        if (kIOReturnSuccess == result) {
            printf("%d: %s connection established.\n", __LINE__, [[device name] UTF8String]);
        } else {
            printf("%d: %s connection could not be established. removing.\n", __LINE__, [[device name] UTF8String]);
            [devs removeObjectAtIndex: i--];
        }
    }
    
    // get the list of services for each device.
    denum = [devs objectEnumerator];
    while (device = [denum nextObject]) {
        result = [device performSDPQuery: [[[SDPQuery alloc] init] autorelease]];
        assert(kIOReturnSuccess == result);
        printf("%d: sdp query started.\n", __LINE__);
        
        CFRunLoopRun();
        
        NSArray *foo = [device getServices];
        IOBluetoothSDPServiceRecord *r;
        int i;
        for (i = 0; i < [foo count]; i++) {
            r = [foo objectAtIndex: i];
            NSLog(@"%d: service: %@.\n", __LINE__, r);
        }        
    }
    
    // close connection
    // NB: [devs makeObjectsPerformSelector: @selector(closeConnection)]; also works.
    denum = [devs objectEnumerator];
    while (device = [denum nextObject]) {
        result = [device closeConnection];
        assert(kIOReturnSuccess == result);
        printf("%d: %s connection closed.\n", __LINE__, [[device name] UTF8String]);
    };
    
    // }
    [pool drain];

    return 0;
}
