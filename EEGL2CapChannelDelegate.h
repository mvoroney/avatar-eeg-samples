
#ifndef EEG_L2CAP_CHANNEL_DELEGATE_H
#define EEG_L2CAP_CHANNEL_DELEGATE_H

#import <Foundation/NSObject.h>
#import <IOKit/IOReturn.h>

#import <IOBluetooth/objc/IOBluetoothL2CAPChannel.h>

@interface EEGL2CapChannelDelegate : NSObject<IOBluetoothL2CAPChannelDelegate>

- (void)l2capChannelData:(IOBluetoothL2CAPChannel*)l2capChannel data:(void *)dataPointer length:(size_t)dataLength;
- (void)l2capChannelOpenComplete:(IOBluetoothL2CAPChannel*)l2capChannel status:(IOReturn)error;
- (void)l2capChannelClosed:(IOBluetoothL2CAPChannel*)l2capChannel;
- (void)l2capChannelReconfigured:(IOBluetoothL2CAPChannel*)l2capChannel;
- (void)l2capChannelWriteComplete:(IOBluetoothL2CAPChannel*)l2capChannel refcon:(void*)refcon status:(IOReturn)error;
- (void)l2capChannelQueueSpaceAvailable:(IOBluetoothL2CAPChannel*)l2capChannel;

@end

#endif /* EEG_L2CAP_CHANNEL_DELEGATE_H */
