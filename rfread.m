

#include <stdio.h>

#import <Foundation/Foundation.h>
#import <Foundation/NSAutoreleasePool.h>
#import <IOKit/IOReturn.h>

#import <IOBluetooth/objc/IOBluetoothDevice.h>
#import <IOBluetooth/objc/IOBluetoothSDPServiceRecord.h>

#import "EEGL2CapChannelDelegate.h"
#import "EEGRFCommChannelDelegate.h"
#import "IOReturnString.h"

@interface SDPQuery : NSObject

- (void)sdpQueryComplete:(IOBluetoothDevice*) dev status: (IOReturn) stat;

@end

@implementation SDPQuery

- (void)sdpQueryComplete:(IOBluetoothDevice*) dev status: (IOReturn) stat {
    assert(kIOReturnSuccess == stat);
    printf("%d: sdp query completed successfully.\n", __LINE__);
    CFRunLoopStop(CFRunLoopGetCurrent());
}

@end

// NB: this is the address of the Avatar EEG device. change as needed.
//static BluetoothDeviceAddress devAddr = {{0x00, 0x16, 0xa4, 0x03, 0x22, 0x7c}};
static BluetoothDeviceAddress devAddr = {{0x00, 0x16, 0xa4, 0x04, 0xd5, 0x8e}};

int main (int argc, const char * argv[]) {
    
    // works with xcode 3
    NSAutoreleasePool * pool = [NSAutoreleasePool new];
        
    IOReturn result;
    IOBluetoothRFCOMMChannel *eegRFChan;
    EEGRFCommChannelDelegate *eegRFDel;
    IOBluetoothDevice *device;
    
    device = [IOBluetoothDevice withAddress: &devAddr];
    
    result = [device openConnection];
    assert(kIOReturnSuccess == result);
    printf("%d: %s connection established.\n", __LINE__, [[device name] UTF8String]);
    
    // get available services.
    result = [device performSDPQuery: [[[SDPQuery alloc] init] autorelease]];
    assert(kIOReturnSuccess == result);
    printf("%d: sdp query started.\n", __LINE__);
        
    CFRunLoopRun();
    
    // NB: the immediately following code expects that there was only one service.
    // \todo fix this.
    NSArray *foo = [device getServices];
    IOBluetoothSDPServiceRecord *r;
    int i;
    for (i = 0; i < [foo count]; i++) {
        r = [foo objectAtIndex: i];
        NSLog(@"%d: service: %@.\n", __LINE__, r);
    }
    
    eegRFDel = [[[EEGRFCommChannelDelegate alloc] init] autorelease];
    
    BluetoothRFCOMMChannelID rfChanID;
    result = [r getRFCOMMChannelID: &rfChanID];
    assert(kIOReturnSuccess == result);
    printf("%d: retrieved RFCOMM Channel ID: %d.\n", __LINE__, rfChanID);
    
    printf("%d: device open: connected? %d, paired? %d.\n", __LINE__, [device isConnected], [device isPaired]);
    
    result = [device openRFCOMMChannelAsync: &eegRFChan withChannelID: rfChanID delegate: eegRFDel];
    
    if (kIOReturnSuccess == result) {
        printf("%d: channel opened successfully.\n", __LINE__);
    } else {
        printf("%d: failed to open channel. reason: 0x%x - %s.\n", __LINE__, result, ioReturnString(result));
        assert(NO);
    }
    
    CFRunLoopRun();
    
    // close connection
    result = [device closeConnection];
    assert(kIOReturnSuccess == result);
    printf("%d: %s connection closed.\n", __LINE__, [[device name] UTF8String]);
    
    [pool drain];
    
    return 0;
}
