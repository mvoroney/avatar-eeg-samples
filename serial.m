

#include <stdio.h>
#import <IOKit/serial/IOSerialKeys.h>
#include <fcntl.h>
#include <string.h>


static int serialConnect(const char * path) {
	int fd;
	
	fd = open(path, O_RDWR | O_NOCTTY | O_NDELAY);
	if (0 > fd) {
		printf("%d error opening: %s, error: %d: %s.\n", __LINE__, path, errno, strerror(errno));
	}
	
	return fd;
	
}

/*
 * taken from: http://developer.apple.com/library/mac/#documentation/DeviceDrivers/Conceptual/WorkingWSerial/WWSerial_SerialDevs/SerialDevices.html
 */
static kern_return_t findSerialDev(io_iterator_t *matchingServices)
{
    kern_return_t       kernResult;
    mach_port_t         masterPort;
    CFMutableDictionaryRef  classesToMatch;
	
    kernResult = IOMasterPort(MACH_PORT_NULL, &masterPort);
    if (KERN_SUCCESS != kernResult)
    {
        printf("IOMasterPort returned %d\n", kernResult);
		return kernResult;
    }
	
    // Serial devices are instances of class IOSerialBSDClient.
    classesToMatch = IOServiceMatching(kIOSerialBSDServiceValue);
    if (classesToMatch == NULL)
    {
        printf("IOServiceMatching returned a NULL dictionary.\n");
    }
    else {
        CFDictionarySetValue(classesToMatch,
                             CFSTR(kIOSerialBSDTypeKey),
                             CFSTR(kIOSerialBSDRS232Type));
	}
	
    kernResult = IOServiceGetMatchingServices(masterPort, classesToMatch, matchingServices);
    if (KERN_SUCCESS != kernResult)
    {
        printf("IOServiceGetMatchingServices returned %d\n", kernResult);
		return kernResult;
    }
	
    return kernResult;
}

/*
 * taken from: http://developer.apple.com/library/mac/#documentation/DeviceDrivers/Conceptual/WorkingWSerial/WWSerial_SerialDevs/SerialDevices.html
 */
static kern_return_t serialDevPath(io_iterator_t serialPortIterator, char *deviceFilePath, CFIndex maxPathSize)
{
    io_object_t     modemService;
    kern_return_t   kernResult = KERN_FAILURE;
    Boolean     modemFound = false;
	
    // Initialize the returned path
    *deviceFilePath = '\0';
	
    // Iterate across all modems found. In this example, we exit after
    // finding the first modem.
	
    while ((!modemFound) && (modemService = IOIteratorNext(serialPortIterator)))
    {
        CFTypeRef   deviceFilePathAsCFString;
		
		// Get the callout device's path (/dev/cu.xxxxx).
		// The callout device should almost always be
		// used. You would use the dialin device (/dev/tty.xxxxx) when
		// monitoring a serial port for
		// incoming calls, for example, a fax listener.
		
		deviceFilePathAsCFString = IORegistryEntryCreateCFProperty(modemService,
																   CFSTR(kIODialinDeviceKey),
																   kCFAllocatorDefault,
																   0);
        if (deviceFilePathAsCFString)
        {
            Boolean result;
			
			// Convert the path from a CFString to a NULL-terminated C string
			// for use with the POSIX open() call.
			
			result = CFStringGetCString(deviceFilePathAsCFString,
                                        deviceFilePath,
                                        maxPathSize,
                                        kCFStringEncodingASCII);
            CFRelease(deviceFilePathAsCFString);
			
            if (result && NULL != strstr(deviceFilePath, "Laird"))
            {
                printf("BSD path: %s", deviceFilePath);
                modemFound = true;
                kernResult = KERN_SUCCESS;
            }
        }
		
        printf("\n");
		
        // Release the io_service_t now that we are done with it.
		
		(void) IOObjectRelease(modemService);
    }
	
    return kernResult;
}

int main (int argc, const char * argv[]) {
    
    int fd;
	char sDevPath[255];
    
	io_iterator_t serialServices;
	findSerialDev(&serialServices);
	serialDevPath(serialServices, sDevPath, 255);
	
	fd = serialConnect(sDevPath);
	
	close(fd);
    	
    return 0;
}
