
#import <IOBluetooth/objc/IOBluetoothL2CAPChannel.h>

#include "EEGL2CapChannelDelegate.h"

@implementation EEGL2CapChannelDelegate

- (void)l2capChannelData:(IOBluetoothL2CAPChannel*)l2capChannel data:(void *)dataPointer length:(size_t)dataLength {
    printf("%d: data recieved: \n", __LINE__);
    unsigned char *di = (unsigned char*)dataPointer;
    unsigned char *end = di + dataLength;
    
    while (di < end) {
        putchar(*di++);
    }
}

- (void)l2capChannelOpenComplete:(IOBluetoothL2CAPChannel*)l2capChannel status:(IOReturn)error {
    assert(kIOReturnSuccess == error);
    printf("%d: channel opened successfully.\n", __LINE__);
}

- (void)l2capChannelClosed:(IOBluetoothL2CAPChannel*)l2capChannel {
    printf("%d: channel closed successfully.\n", __LINE__);
}

- (void)l2capChannelReconfigured:(IOBluetoothL2CAPChannel*)l2capChannel {
    printf("%d: channel reconfigured.\n", __LINE__);
}

- (void)l2capChannelWriteComplete:(IOBluetoothL2CAPChannel*)l2capChannel refcon:(void*)refcon status:(IOReturn)error {
    assert(kIOReturnSuccess == error);
    printf("%d: write completed successfully.\n", __LINE__);
}

- (void)l2capChannelQueueSpaceAvailable:(IOBluetoothL2CAPChannel*)l2capChannel {
    printf("%d: queue space available.\n", __LINE__);
}


@end
