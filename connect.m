#include <stdio.h>

#import <Foundation/NSObject.h>
#import <Foundation/Foundation.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSEnumerator.h>
#import <Foundation/NSThread.h>
#import <IOkit/IOReturn.h>

#import <IOBluetooth/BluetoothAssignedNumbers.h>
#import <IOBluetooth/IOBluetooth.h>
#import <IOBluetooth/objc/IOBluetoothDevice.h>
#import <IOBluetooth/objc/IOBluetoothDeviceInquiry.h>
#import <IOBluetooth/objc/IOBluetoothRFCOMMChannel.h>




@interface Discoverer : NSObject
    {}

    - (void)deviceInquiryComplete:(IOBluetoothDeviceInquiry *)sender error:(IOReturn)error aborted:(BOOL)aborted;
    - (void)deviceInquiryDeviceFound:(IOBluetoothDeviceInquiry *)sender device:(IOBluetoothDevice *)device;
@end

@implementation Discoverer

    - (void)deviceInquiryComplete:(IOBluetoothDeviceInquiry *)sender error:(IOReturn)error aborted:(BOOL)aborted {
        CFRunLoopStop(CFRunLoopGetCurrent());
    }

    - (void)deviceInquiryDeviceFound:(IOBluetoothDeviceInquiry *)sender device:(IOBluetoothDevice *)device {
        printf("%d: discovered %s name=%s, majorService=%x, majorClass=%x, minorClass=%x\n", 
               __LINE__, 
               [[device getAddressString] UTF8String], 
               [[device name] UTF8String],
               [device getServiceClassMajor],
               [device getDeviceClassMajor],
               [device getDeviceClassMinor]
        );
    }

@end


@interface SDPQuery : NSObject
- (void)sdpQueryComplete:(IOBluetoothDevice*) dev status: (IOReturn) stat;

@end

@implementation SDPQuery

- (void)sdpQueryComplete:(IOBluetoothDevice*) dev status: (IOReturn) stat {
    assert(kIOReturnSuccess == stat);
    printf("%d: sdp query completed successfully.\n", __LINE__);
    CFRunLoopStop(CFRunLoopGetCurrent());
}

@end


@interface EEGRFCommChannelDelegate : NSObject<IOBluetoothRFCOMMChannelDelegate>
{
    
}

- (void)rfcommChannelData:(IOBluetoothRFCOMMChannel*)rfcommChannel data:(void *)dataPointer length:(size_t)dataLength;
- (void)rfcommChannelOpenComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel status:(IOReturn)error;
- (void)rfcommChannelClosed:(IOBluetoothRFCOMMChannel*)rfcommChannel;
- (void)rfcommChannelControlSignalsChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel;
- (void)rfcommChannelFlowControlChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel;
- (void)rfcommChannelQueueSpaceAvailable:(IOBluetoothRFCOMMChannel*)rfcommChannel;

@end
 
@implementation EEGRFCommChannelDelegate

- (void)rfcommChannelData:(IOBluetoothRFCOMMChannel*)rfcommChannel data:(void *)dataPointer length:(size_t)dataLength {
    printf("%d: channel data recieved.\n", __LINE__);
    int i;
    for (i = 0; i < dataLength; i++) {
        putchar(((unsigned char*)dataPointer)[i]);
    }
}

- (void)rfcommChannelOpenComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel status:(IOReturn)error {
    assert(kIOReturnSuccess == error);
    printf("%d: opened channel successfully.\n", __LINE__);
}

- (void)rfcommChannelClosed:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    printf("%d: closed channel successfully.\n", __LINE__);
    CFRunLoopStop(CFRunLoopGetCurrent());
}

- (void)rfcommChannelControlSignalsChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    printf("%d: control signal changed.\n", __LINE__);
}

- (void)rfcommChannelFlowControlChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    printf("%d: flow control changed.\n", __LINE__);
}


- (void)rfcommChannelQueueSpaceAvailable:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    printf("%d: queue space available.\n", __LINE__);
}

@end


@interface EEGL2CapChannelDelegate : NSObject<IOBluetoothL2CAPChannelDelegate>

- (void)l2capChannelData:(IOBluetoothL2CAPChannel*)l2capChannel data:(void *)dataPointer length:(size_t)dataLength;
- (void)l2capChannelOpenComplete:(IOBluetoothL2CAPChannel*)l2capChannel status:(IOReturn)error;
- (void)l2capChannelClosed:(IOBluetoothL2CAPChannel*)l2capChannel;
- (void)l2capChannelReconfigured:(IOBluetoothL2CAPChannel*)l2capChannel;
- (void)l2capChannelWriteComplete:(IOBluetoothL2CAPChannel*)l2capChannel refcon:(void*)refcon status:(IOReturn)error;
- (void)l2capChannelQueueSpaceAvailable:(IOBluetoothL2CAPChannel*)l2capChannel;

@end

@implementation EEGL2CapChannelDelegate

- (void)l2capChannelData:(IOBluetoothL2CAPChannel*)l2capChannel data:(void *)dataPointer length:(size_t)dataLength {
    printf("%d: data recieved: \n", __LINE__);
    unsigned char *di = (unsigned char*)dataPointer;
    unsigned char *end = di + dataLength;
    
    while (di < end) {
        putchar(*di++);
    }
}

- (void)l2capChannelOpenComplete:(IOBluetoothL2CAPChannel*)l2capChannel status:(IOReturn)error {
    assert(kIOReturnSuccess == error);
    printf("%d: channel opened successfully.\n", __LINE__);
}

- (void)l2capChannelClosed:(IOBluetoothL2CAPChannel*)l2capChannel {
    printf("%d: channel closed successfully.\n", __LINE__);
}

- (void)l2capChannelReconfigured:(IOBluetoothL2CAPChannel*)l2capChannel {
    printf("%d: channel reconfigured.\n", __LINE__);
}

- (void)l2capChannelWriteComplete:(IOBluetoothL2CAPChannel*)l2capChannel refcon:(void*)refcon status:(IOReturn)error {
    assert(kIOReturnSuccess == error);
    printf("%d: write completed successfully.\n", __LINE__);
}

- (void)l2capChannelQueueSpaceAvailable:(IOBluetoothL2CAPChannel*)l2capChannel {
    printf("%d: queue space available.\n", __LINE__);
}


@end



int main (int argc, const char * argv[]) {
    
    // works with xcode 3
    NSAutoreleasePool * pool = [NSAutoreleasePool new];
    
    // needs xcode 4
    //@autoreleasepool {
        
    IOReturn result;
    Discoverer *dis = [[Discoverer new] autorelease];
    NSArray *devs;
    //IOBluetoothRFCOMMChannel *eegRFChan;
    IOBluetoothL2CAPChannel * eegL2Chan;
    EEGRFCommChannelDelegate *eegRFDel;
    EEGL2CapChannelDelegate * eegL2Del;
    NSEnumerator *denum;
    IOBluetoothDevice *device;
    IOBluetoothDeviceInquiry *diref = [[IOBluetoothDeviceInquiry new] autorelease];
    
    [diref setDelegate:dis];
    [diref setInquiryLength:10];
    [diref setSearchCriteria:kBluetoothServiceClassMajorAny 
            majorDeviceClass:kBluetoothDeviceClassMajorUnclassified 
            minorDeviceClass:kBluetoothDeviceClassMinorComputerUnclassified];

    result = [diref start];
    assert(kIOReturnSuccess == result);
    printf("%d: start succeeded.\n", __LINE__);

    // start device search loop.
    CFRunLoopRun();
        
    result = [diref stop];
    assert(kIOReturnSuccess == result);
    printf("%d: stop succeeded.\n", __LINE__);
    
    devs = [diref foundDevices];
    
    // open conneciton
    // NB: [devs makeObjectsPerformSelector: @selector(openConnection)]; also works.
    denum = [devs objectEnumerator];
    while (device = [denum nextObject]) {
        result = [device openConnection];
        assert(kIOReturnSuccess == result);
        printf("%d: %s connection established.\n", __LINE__, [[device name] UTF8String]);
    }
    
    //This block is not actually querying the avatar - will investigate later
    denum = [devs objectEnumerator];
    while (device = [denum nextObject]) {
        result = [device performSDPQuery: [[SDPQuery alloc] init]];
        assert(kIOReturnSuccess == result);
        printf("%d: sdp query started.\n", __LINE__);
        
        CFRunLoopRun();
    }
    
    NSArray *foo = [[devs objectAtIndex: 0] getServices];
    IOBluetoothSDPServiceRecord *r;
    int i;
    for (i = 0; i < [foo count]; i++) {
        r = [foo objectAtIndex: i];
        printf("%d: service: %s.\n", __LINE__, [[r getServiceName] UTF8String]);
    }
    BluetoothL2CAPPSM psm;
    result = [r getL2CAPPSM: &psm];
    assert(kIOReturnSuccess == result);
    
    
    eegL2Del = [[EEGL2CapChannelDelegate alloc] init];
    
    [device openL2CAPChannelSync: &eegL2Chan withPSM: psm delegate: eegL2Del];
    
     
     eegRFDel = [[EEGRFCommChannelDelegate alloc] init];
    //[device openRFCOMMChannelSync: &eegRFChan withChannelID: 1 delegate: eegDelegate];
    
    
    
    //CFRunLoopRun();
    
    
    // close connection
    // NB: [devs makeObjectsPerformSelector: @selector(closeConnection)]; also works.
    denum = [devs objectEnumerator];
    while (device = [denum nextObject]) {
        result = [device closeConnection];
        assert(kIOReturnSuccess == result);
        printf("%d: %s connection closed.\n", __LINE__, [[device name] UTF8String]);
    };
    
    // }
    [pool drain];

    return 0;
}
