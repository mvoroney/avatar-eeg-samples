
#ifndef EEG_RFCOMM_CHANNEL_DELEGATE_H
#define EEG_RFCOMM_CHANNEL_DELEGATE_H

#import <Foundation/NSObject.h>
#import <IOkit/IOReturn.h>

#import <IOBluetooth/objc/IOBluetoothRFCOMMChannel.h>

@interface EEGRFCommChannelDelegate : NSObject<IOBluetoothRFCOMMChannelDelegate> {
    NSUInteger remainingSamples;
}

-(id) init;

- (void)rfcommChannelData:(IOBluetoothRFCOMMChannel*)rfcommChannel data:(void *)dataPointer length:(size_t)dataLength;
- (void)rfcommChannelOpenComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel status:(IOReturn)error;
- (void)rfcommChannelClosed:(IOBluetoothRFCOMMChannel*)rfcommChannel;
- (void)rfcommChannelControlSignalsChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel;
- (void)rfcommChannelFlowControlChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel;
- (void)rfcommChannelQueueSpaceAvailable:(IOBluetoothRFCOMMChannel*)rfcommChannel;

@end

#endif /* EEG_RFCOMM_CHANNEL_DELEGATE_H */
