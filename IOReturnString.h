
#ifndef IO_RETURN_STRING_H
#define IO_RETURN_STRING_H

#include <stdint.h>

const char * ioReturnString(uint32_t error);

#endif /* IO_RETURN_STRING_H */
